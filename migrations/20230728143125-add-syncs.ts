"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable("sync_sources", {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        url: Sequelize.DataTypes.TEXT,
        name: Sequelize.DataTypes.TEXT,
        image: Sequelize.DataTypes.TEXT,
        sourceType: Sequelize.DataTypes.STRING,
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        lastCrawledAt: {
          allowNull: true,
          type: Sequelize.DATE,
        },
        lastCrawlErrorMessage: {
          allowNull: true,
          type: Sequelize.DataTypes.TEXT,
        },
        userId: {
          type: Sequelize.DataTypes.INTEGER,
          onDelete: "CASCADE",
          references: {
            model: {
              tableName: "users",
            },
            key: "id",
          },
        },
      });
      await queryInterface.createTable("sync_relations", {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        sourceId: {
          type: Sequelize.DataTypes.INTEGER,
          onDelete: "CASCADE",
          references: {
            model: {
              tableName: "sync_sources",
            },
            key: "id",
          },
        },
        destinationId: {
          type: Sequelize.DataTypes.INTEGER,
          onDelete: "CASCADE",
          references: {
            model: {
              tableName: "destinations",
            },
            key: "id",
          },
        },
        refreshRate: Sequelize.DataTypes.STRING,
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        lastSyncedAt: {
          allowNull: true,
          type: Sequelize.DATE,
        },
        lastSyncErrorMessage: {
          allowNull: true,
          type: Sequelize.DataTypes.TEXT,
        },
      });
      await queryInterface.addIndex(
        "sync_relations",
        ["sourceId", "destinationId"],
        {
          unique: true,
        },
      );
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.dropTable("sync_relations");
      await queryInterface.dropTable("sync_sources");
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
};
