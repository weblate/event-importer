"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable("sync_relation_action", {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        contentHash: {
          type: Sequelize.DataTypes.TEXT,
          allowNull: false,
        },
        sourceUrl: {
          type: Sequelize.DataTypes.TEXT,
          allowNull: false,
        },
        publishedUrl: {
          type: Sequelize.DataTypes.TEXT,
          allowNull: false,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        syncRelationId: {
          type: Sequelize.DataTypes.INTEGER,
          onDelete: "CASCADE",
          references: {
            model: {
              tableName: "sync_relations",
            },
            key: "id",
          },
        },
      });
      await queryInterface.addIndex(
        "sync_relation_action",
        ["sourceUrl", "publishedUrl"],
        {
          unique: true,
        },
      );

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.dropTable("sync_relation_action");
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
};
