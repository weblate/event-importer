"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable("mbz_oauth_applications", {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        name: Sequelize.DataTypes.TEXT,
        hostname: Sequelize.DataTypes.TEXT,
        client_id: Sequelize.DataTypes.TEXT,
        client_secret: Sequelize.DataTypes.TEXT,
        scopes: Sequelize.DataTypes.TEXT,
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      });
      await queryInterface.createTable("mbz_oauth_application_tokens", {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        federatedIdentity: Sequelize.DataTypes.TEXT,
        accessToken: Sequelize.DataTypes.TEXT,
        refreshToken: Sequelize.DataTypes.TEXT,
        scopes: Sequelize.DataTypes.TEXT,
        appId: {
          type: Sequelize.DataTypes.INTEGER,
          onDelete: "CASCADE",
          references: {
            model: {
              tableName: "mbz_oauth_applications",
            },
            key: "id",
          },
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        userId: {
          type: Sequelize.DataTypes.INTEGER,
          onDelete: "CASCADE",
          references: {
            model: {
              tableName: "users",
            },
            key: "id",
          },
        },
      });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.dropTable("mbz_oauth_application_tokens");
      await queryInterface.dropTable("mbz_oauth_applications");
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
};
