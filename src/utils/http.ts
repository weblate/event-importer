import fetch, { RequestInit, Response } from "node-fetch";

export async function downloadURL(
  url: string,
  options: RequestInit | undefined = undefined,
): Promise<Response> {
  console.debug(`downloading ${url}`);
  return await fetch(url, options);
}

export async function headURL(url: string): Promise<Response> {
  return downloadURL(url, { method: "HEAD" });
}

export async function urlContentType(url: string): Promise<string | null> {
  const res = await headURL(url);
  return res.headers.get("content-type");
}
