import { EventJoinOptions, IEvent } from "../models/event.js";
import { IOrganizer } from "../models/organizer.js";
import { downloadURL } from "../utils/http.js";
import { ImportResult, Importer } from "./importer.js";
import ICAL from "ical.js";
import type { Component } from "ical.js";

export class ICalendar implements Importer {
  private component: Component | null = null;
  private body: string | undefined = undefined;

  async downloadData(url: string): Promise<void> {
    await this.getComponent(url);
  }

  async importEvent(url: string): Promise<ImportResult<IEvent>> {
    if (!this.component) return { result: null, body: this.body ?? "" };
    const vevent = this.component.getFirstSubcomponent("vevent");
    if (!vevent) return { result: null, body: this.body ?? "" };
    return {
      result: this.convertVEvent(vevent),
      body: this.body ?? "",
    };
  }
  async importEvents(url: string): Promise<ImportResult<IOrganizer>> {
    if (!this.component) return { result: null, body: this.body ?? "" };
    const vevents = this.component.getAllSubcomponents("vevent");
    return {
      result: {
        name: "",
        url,
        events: vevents
          .map((vevent: any) => this.convertVEvent(vevent))
          .filter(this.isEventFuture),
      },
      body: this.body ?? "",
    };
  }

  countSubComponents(): number {
    if (!this.component) return 0;
    return this.component.getAllSubcomponents("vevent").length;
  }

  private async getComponent(url: string) {
    this.body = await (await downloadURL(url)).text();
    const jCalData = ICAL.parse(this.body);
    this.component = new ICAL.Component(jCalData);
  }

  private convertVEvent(vEvent: Component): IEvent {
    const eventUrl = vEvent.getFirstPropertyValue("url");
    return {
      name: vEvent.getFirstPropertyValue("summary"),
      description: vEvent.getFirstPropertyValue("description"),
      startDate: new Date(vEvent.getFirstPropertyValue("dtstart")),
      endDate: vEvent.getFirstPropertyValue("dtend")
        ? new Date(vEvent.getFirstPropertyValue("dtend"))
        : undefined,
      url: eventUrl ?? `data:ics:uid:${vEvent.getFirstPropertyValue("uid")}`,
      joinOptions: EventJoinOptions.EXTERNAL,
      externalParticipationUrl: eventUrl,
      eventStatus: vEvent.getFirstPropertyValue("status"),
      organizer: vEvent.getFirstPropertyValue("organizer"),
      tags: vEvent.getFirstProperty("categories")?.getValues() ?? [],
      location: {
        name: vEvent.getFirstPropertyValue("location"),
        address: {
          streetAddress: null,
          addressLocality: null,
          addressRegion: null,
          addressCountry: null,
          geom: this.convertGeo(vEvent.getFirstPropertyValue("geo")),
        },
      },
      // TODO add TZ from X-WR-TIMEZONE or TZID
    };
  }

  private isEventFuture(event: IEvent): boolean {
    return event.startDate > new Date();
  }

  private convertGeo(geoProperty: [number, number] | undefined): string | null {
    if (!geoProperty) return null;
    return `${geoProperty[1]};${geoProperty[0]}`;
  }
}
