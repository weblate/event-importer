import {
  randomBytes,
  createHash,
  createCipheriv,
  createDecipheriv,
} from "node:crypto";

export const encrypt = (plainText: string, password: string) => {
  try {
    const iv = randomBytes(16);
    const key = createHash("sha256")
      .update(password)
      .digest("base64")
      .slice(0, 32);
    const cipher = createCipheriv("aes-256-cbc", key, iv);

    let encrypted = cipher.update(plainText);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return { iv: iv.toString("hex"), encryptedText: encrypted.toString("hex") };
  } catch (error) {
    console.error(error);
  }
};

export const decrypt = (
  encryptedText: string,
  ivText: string,
  password: string,
) => {
  try {
    const iv = Buffer.from(ivText as string, "hex");

    const encryptedData = Buffer.from(encryptedText, "hex");
    const key = createHash("sha256")
      .update(password)
      .digest("base64")
      .slice(0, 32);
    const decipher = createDecipheriv("aes-256-cbc", key, iv);

    const decrypted = decipher.update(encryptedData);
    const decryptedText = Buffer.concat([decrypted, decipher.final()]);
    return decryptedText.toString();
  } catch (error) {
    console.error(error);
  }
};
