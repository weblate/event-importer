import { Destination } from "../models/destination.js";
import { IEvent } from "../models/event.js";
import {
  MobilizonOAuthApplication,
  MobilizonOAuthApplicationToken,
} from "../models/mobilizon_oauth_application.model.js";
import { DESTINATION_TYPES } from "../models/sync.js";
import { WebHook } from "../models/webhook.js";
import {
  EventVariables,
  createEvent,
  searchEventByUrl,
  updateEvent,
  userMemberships,
  userProfiles,
} from "./mobilizon.js";
import { convertStatus, eventOptions } from "./mobilizon/converter.js";
import { IActor } from "./mobilizon/interfaces/actor.js";
import { deduplicateMemberships } from "./mobilizon/interfaces/member.js";
import { publishWebhookEvent } from "./webhook.js";

export async function publishEvent(
  event: IEvent,
  destination: Destination,
  userId: number,
) {
  console.info(
    `Publishing event ${event.name} (${event.url}) to ${destination.name} (${destination.targetUrl})`,
  );
  switch (destination?.type) {
    case DESTINATION_TYPES.MBZ_DESTINATION:
      const mobilizonOAuthAppToken = await getMobilizonTokens(userId);
      if (!mobilizonOAuthAppToken) {
        console.info(
          `Could not find mobilizonOAuthAppToken for user ${userId}`,
        );
        return;
      }

      const hostname = mobilizonOAuthAppToken.app.hostname;

      const { organizer, attributedTo } = await getAuthors(
        hostname,
        mobilizonOAuthAppToken,
        destination.targetUrl,
      );

      const variables: EventVariables = {
        ...event,
        organizerActorId: organizer.id,
        attributedToId: attributedTo?.id,
        title: event.name,
        beginsOn: event.startDate,
        endsOn: event.endDate,
        status: convertStatus(event.eventStatus),
        options: eventOptions(event),
      };

      if (event.location?.address?.geom) {
        variables.physicalAddress = {
          street: event.location.address?.streetAddress,
          locality: event.location.address?.addressLocality,
          geom: event.location.address?.geom,
          description: event.location.name,
          country: event.location.address?.addressCountry,
          region: event.location.address?.addressRegion,
          // postalCode: event.location.address.addressRegion
        };
      }

      const { createEvent: publishedEvent } = await createEvent(
        hostname,
        mobilizonOAuthAppToken,
        variables,
      );
      console.info(`Event ${event.name} published`);
      return publishedEvent;

    case DESTINATION_TYPES.WEBHOOK_DESTINATION:
      const webhook = await WebHook.findOne({
        where: {
          targetUrl: destination.targetUrl,
          userId: userId,
        },
      });
      if (!webhook) {
        throw "Webhook not found";
      }
      await publishWebhookEvent(event, webhook, "create");
      break;
    default:
      break;
  }
}

export async function updatePublishedEvent(
  event: IEvent,
  publishedUrl: string,
  destination: Destination,
  userId: number,
) {
  console.info(
    `Updating event ${event.name} (${event.url}) to ${destination.name} (${destination.targetUrl})`,
  );
  switch (destination?.type) {
    case DESTINATION_TYPES.MBZ_DESTINATION:
      const mobilizonOAuthAppToken = await getMobilizonTokens(userId);
      if (!mobilizonOAuthAppToken) {
        console.info(
          `Could not find mobilizonOAuthAppToken for user ${userId}`,
        );
        return;
      }

      const hostname = mobilizonOAuthAppToken.app.hostname;

      const { organizer, attributedTo } = await getAuthors(
        hostname,
        mobilizonOAuthAppToken,
        destination.targetUrl,
      );

      // search Event by URL in destination instance, and find it's ID
      const resultExistingEvent = await searchEventByUrl(
        hostname,
        mobilizonOAuthAppToken,
        publishedUrl,
      );
      if (resultExistingEvent.interact.__typename !== "Event") {
        console.info(`Could not find existing event to update`);
        return;
      }
      const existingEventId = resultExistingEvent.interact.id;

      const { updateEvent: updatedEvent } = await updateEvent(
        hostname,
        mobilizonOAuthAppToken,
        {
          ...event,
          organizerActorId: organizer.id,
          attributedToId: attributedTo?.id,
          title: event.name,
          beginsOn: event.startDate,
          id: existingEventId,
        },
      );
      return updatedEvent;

    case DESTINATION_TYPES.WEBHOOK_DESTINATION:
      const webhook = await WebHook.findOne({
        where: {
          targetUrl: destination.targetUrl,
          userId: userId,
        },
      });
      if (!webhook) {
        throw "Webhook not found";
      }
      await publishWebhookEvent(event, webhook, "update");
      break;
    default:
      break;
  }
}

async function getMobilizonTokens(userId: number) {
  return MobilizonOAuthApplicationToken.findOne({
    where: {
      userId: userId,
    },
    order: [["createdAt", "DESC"]],
    limit: 1,
    group: ["federatedIdentity", "MobilizonOAuthApplicationToken.id", "app.id"],
    include: MobilizonOAuthApplication,
  });
}

async function getAuthors(
  hostname: string,
  mobilizonOAuthAppToken: MobilizonOAuthApplicationToken,
  targetUrl: string,
) {
  let organizer: IActor | undefined;
  let attributedTo: IActor | undefined;

  const profiles =
    (await userProfiles(hostname, mobilizonOAuthAppToken)).actors?.reduce(
      (profiles, profile) => {
        profiles[profile.url] = profile;
        return profiles;
      },
      {} as Record<string, IActor>,
    ) ?? {};
  if (profiles[targetUrl]) {
    organizer = profiles[targetUrl];
    console.debug("Publishing to profile", organizer);
    return { organizer, attributedTo };
  }

  const memberships = await userMemberships(hostname, mobilizonOAuthAppToken);

  console.debug("Current user memberships", memberships);

  if (!memberships) {
    throw `Could not find memberships for connected MBZ user`;
  }
  const deduplicatedMemberships = deduplicateMemberships(memberships);

  const membership = deduplicatedMemberships[targetUrl];
  if (!membership) {
    throw `Could not identify matching membership for destination target URL`;
  }
  organizer = membership.actor;
  attributedTo = membership.parent;
  console.debug("Publishing to group with actor", attributedTo, organizer);
  return { organizer, attributedTo };
}
