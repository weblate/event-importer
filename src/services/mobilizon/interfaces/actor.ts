export interface IActor {
  id: string;
  avatar: {
    id: string;
    url: string;
  };
  type: string;
  preferredUsername: string;
  name: string;
  domain: string;
  summary: string;
  url: string;
}
