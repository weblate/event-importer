import { gql } from "graphql-request";
import { ACTOR_FRAGMENT } from "./actor.js";
import { TAG_FRAGMENT } from "./tag.js";
import { ADDRESS_FRAGMENT } from "./address.js";

export const memberships = gql`
  query LoggedUserMemberships(
    $membershipName: String
    $page: Int
    $limit: Int
  ) {
    loggedUser {
      id
      memberships(name: $membershipName, page: $page, limit: $limit) {
        total
        elements {
          id
          role
          actor {
            ...ActorFragment
            __typename
          }
          parent {
            ...ActorFragment
            organizedEvents {
              elements {
                id
                title
                picture {
                  id
                  url
                  __typename
                }
                __typename
              }
              total
              __typename
            }
            __typename
          }
          invitedBy {
            ...ActorFragment
            __typename
          }
          __typename
        }
        __typename
      }
      __typename
    }
  }
  ${ACTOR_FRAGMENT}
`;

export const EVENT_OPTIONS_FRAGMENT = gql`
  fragment EventOptions on EventOptions {
    maximumAttendeeCapacity
    remainingAttendeeCapacity
    showRemainingAttendeeCapacity
    anonymousParticipation
    showStartTime
    showEndTime
    timezone
    offers {
      price
      priceCurrency
      url
    }
    participationConditions {
      title
      content
      url
    }
    attendees
    program
    commentModeration
    showParticipationPrice
    hideOrganizerWhenGroupEvent
    isOnline
  }
`;

const FULL_EVENT_FRAGMENT = gql`
  fragment FullEvent on Event {
    id
    uuid
    url
    local
    title
    description
    beginsOn
    endsOn
    status
    visibility
    joinOptions
    externalParticipationUrl
    draft
    language
    category
    picture {
      id
      url
      name
      metadata {
        width
        height
        blurhash
      }
    }
    publishAt
    onlineAddress

    phoneAddress
    physicalAddress {
      ...AdressFragment
    }
    organizerActor {
      ...ActorFragment
    }
    contacts {
      ...ActorFragment
    }
    attributedTo {
      ...ActorFragment
    }
    participantStats {
      going
      notApproved
      participant
    }
    tags {
      ...TagFragment
    }
    relatedEvents {
      id
      uuid
      title
      beginsOn
      status
      language
      picture {
        id
        url
        name
        metadata {
          width
          height
          blurhash
        }
      }
      physicalAddress {
        ...AdressFragment
      }
      organizerActor {
        ...ActorFragment
      }
      attributedTo {
        ...ActorFragment
      }
      options {
        ...EventOptions
      }
      tags {
        ...TagFragment
      }
    }
    options {
      ...EventOptions
    }
    metadata {
      key
      title
      value
      type
    }
  }
  ${ADDRESS_FRAGMENT}
  ${TAG_FRAGMENT}
  ${EVENT_OPTIONS_FRAGMENT}
  ${ACTOR_FRAGMENT}
`;

export const CREATE_EVENT = gql`
  mutation createEvent(
    $organizerActorId: ID!
    $attributedToId: ID
    $title: String!
    $description: String!
    $beginsOn: DateTime!
    $endsOn: DateTime
    $status: EventStatus
    $visibility: EventVisibility
    $joinOptions: EventJoinOptions
    $externalParticipationUrl: String
    $draft: Boolean
    $tags: [String]
    $picture: MediaInput
    $onlineAddress: String
    $phoneAddress: String
    $category: EventCategory
    $physicalAddress: AddressInput
    $options: EventOptionsInput
    $contacts: [Contact]
    $metadata: [EventMetadataInput]
  ) {
    createEvent(
      organizerActorId: $organizerActorId
      attributedToId: $attributedToId
      title: $title
      description: $description
      beginsOn: $beginsOn
      endsOn: $endsOn
      status: $status
      visibility: $visibility
      joinOptions: $joinOptions
      externalParticipationUrl: $externalParticipationUrl
      draft: $draft
      tags: $tags
      picture: $picture
      onlineAddress: $onlineAddress
      phoneAddress: $phoneAddress
      category: $category
      physicalAddress: $physicalAddress
      options: $options
      contacts: $contacts
      metadata: $metadata
    ) {
      ...FullEvent
    }
  }
  ${FULL_EVENT_FRAGMENT}
`;

export const UPDATE_EVENT = gql`
  mutation updateEvent(
    $id: ID!
    $title: String
    $description: String
    $beginsOn: DateTime
    $endsOn: DateTime
    $status: EventStatus
    $visibility: EventVisibility
    $joinOptions: EventJoinOptions
    $externalParticipationUrl: String
    $draft: Boolean
    $tags: [String]
    $picture: MediaInput
    $onlineAddress: String
    $phoneAddress: String
    $organizerActorId: ID
    $attributedToId: ID
    $category: EventCategory
    $physicalAddress: AddressInput
    $options: EventOptionsInput
    $contacts: [Contact]
    $metadata: [EventMetadataInput]
  ) {
    updateEvent(
      eventId: $id
      title: $title
      description: $description
      beginsOn: $beginsOn
      endsOn: $endsOn
      status: $status
      visibility: $visibility
      joinOptions: $joinOptions
      externalParticipationUrl: $externalParticipationUrl
      draft: $draft
      tags: $tags
      picture: $picture
      onlineAddress: $onlineAddress
      phoneAddress: $phoneAddress
      organizerActorId: $organizerActorId
      attributedToId: $attributedToId
      category: $category
      physicalAddress: $physicalAddress
      options: $options
      contacts: $contacts
      metadata: $metadata
    ) {
      ...FullEvent
    }
  }
  ${FULL_EVENT_FRAGMENT}
`;

export const INTERACT = gql`
  query Interact($uri: String!) {
    interact(uri: $uri) {
      ... on Event {
        id
        __typename
      }
      ... on Group {
        id
        __typename
      }
    }
  }
`;
