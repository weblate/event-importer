import { gql } from "graphql-request";
export const LOGGED_USER_INFO = gql`
  query LoggedUser {
    loggedUser {
      id
      email
      defaultActor {
        avatar {
          url
        }
        preferredUsername
        name
      }
    }
  }
`;

export const LOGGED_USER_PROFILES = gql`
  query LoggedUserProfiles {
    loggedUser {
      id
      actors {
        avatar {
          url
        }
        id
        preferredUsername
        name
        url
        summary
      }
    }
  }
`;
