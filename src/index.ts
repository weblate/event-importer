import "dotenv/config";
import express, { Application, NextFunction, Request, Response } from "express";
import { config } from "./config.js";
import { ExceptionsHandler } from "./middlewares/exceptions.handler.js";
import { UnknownRoutesHandler } from "./middlewares/unknownRoutes.handler.js";
import {
  CrawlController,
  DestinationController,
  FacebookController,
  WebhookController,
  MobilizonOAuthController,
  SourceController,
  SyncController,
} from "./controllers/index.js";
import path from "path";
import bodyParser from "body-parser";
import { fileURLToPath } from "url";
import session from "express-session";
import { IUser } from "./models/user.js";
import pug from "pug";
import clip from "text-clipper";
import sessionFileStore from "session-file-store";
import { sequelize } from "./db.js";
import flash from "express-flash-message";
import { I18n } from "i18n";
import { SyncScheduler } from "./schedulers/SyncScheduler.js";
import { FacebookUserSession } from "./services/facebook.js";
import { Destination, SyncRelation, SyncSource } from "./models/index.js";

const FileStore = sessionFileStore(session);

const __filename = fileURLToPath(import.meta.url);

const __dirname = path.dirname(__filename);

const i18n = new I18n({
  locales: ["en", "fr"],
  directory: path.join(__dirname, "locales"),
});

sequelize.authenticate();

declare module "express-session" {
  interface SessionData {
    user: IUser;
    fbUser: FacebookUserSession;
    federatedIdentity?: string;
  }
}

/**
 * On créé une nouvelle "application" express
 */
const app: Application = express();

const facebookEnabled = Boolean(process.env.FB_ENABLED);

app.use(express.static(path.join(__dirname, "../")));

app.use(
  session({
    resave: false,
    saveUninitialized: true,
    secret: config.SECRET,
    store: new FileStore({}),
    cookie: { maxAge: 3600000, secure: false, httpOnly: true },
  }),
);

app.use(
  flash({
    sessionKeyName: "express-flash-message",
  }),
);

/**
 * On dit à Express que l'on souhaite parser le body des requêtes en JSON
 *
 * @example app.post('/', (req) => req.body.prop)
 */
//app.use(express.json())

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(i18n.init);
app.use(function (req: Request, res: Response, next: NextFunction) {
  // express helper for natively supported engines
  // @ts-ignore
  res.locals.__ = res.__ = function () {
    // @ts-ignore
    return i18n.__.apply(req, arguments);
  };

  res.locals.locale = i18n.getLocale(req);

  next();
});

app.use(function (req: Request, res: Response, next: NextFunction) {
  res.locals.templatedTitle = function () {
    // @ts-ignore
    return `${res.__(...arguments)} - ${res.__("Mobilizon importer service")}`;
  };
  next();
});

app.locals.dateFormat = (text: string, locale = "en"): string | null => {
  try {
    return Intl.DateTimeFormat(locale, {
      hour: "numeric",
      minute: "numeric",
      year: "numeric",
      month: "long",
      day: "numeric",
    }).format(new Date(text));
  } catch {
    return null;
  }
};

app.locals.timeFormat = (text: string, locale = "en"): string | null => {
  try {
    return Intl.DateTimeFormat(locale, {
      hour: "numeric",
      minute: "numeric",
    }).format(new Date(text));
  } catch {
    return null;
  }
};

app.locals.dayFormat = (text: string, locale = "en"): string | null => {
  try {
    return Intl.DateTimeFormat(locale, {
      year: "numeric",
      month: "long",
      day: "numeric",
    }).format(new Date(text));
  } catch {
    return null;
  }
};

app.locals.sameDay = (day1: string, day2: string): boolean => {
  const date1 = new Date(day1);
  const date2 = new Date(day2);
  return (
    date1.getFullYear() === date2.getFullYear() &&
    date1.getMonth() === date2.getMonth() &&
    date1.getDate() === date2.getDate()
  );
};

app.locals.clip = (text: string, length = 140): string => {
  return clip.default(text, length, { html: true });
};

app.locals.facebookEnabled = facebookEnabled;

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.engine("pug", pug.renderFile);

app.use((req: Request, res: Response, next: NextFunction) => {
  res.locals.user = req.session.user;
  next();
});

app.use("/crawl", CrawlController);
app.use("/mobilizon/oauth", MobilizonOAuthController);
if (facebookEnabled) {
  app.use("/facebook", FacebookController);
}
app.use("/webhook", WebhookController);
app.use("/syncs", SyncController);
app.use("/sources", SourceController);
app.use("/destinations", DestinationController);

/**
 * Homepage (uniquement necessaire pour cette demo)
 */
app.get("/", async (req: Request, res: Response) => {
  console.debug("home session", req.session);

  if (req.session.user?.id) {
    const syncs = await SyncRelation.findAll({
      include: [
        {
          model: Destination,
          as: "destination",
          required: true,
        },
        {
          model: SyncSource,
          as: "source",
          required: true,
        },
      ],
      where: {
        "$destination.userId$": req.session.user.id,
        "$source.userId$": req.session.user.id,
      },
    });
    return res.render("sync/index", { syncs });
  }

  return res.render("home");
});

app.get("/logout", async (req: Request, res: Response, next: NextFunction) => {
  req.session.user = undefined;
  req.session.save(function (err) {
    if (err) next(err);

    // regenerate the session, which is good practice to help
    // guard against forms of session fixation
    req.session.regenerate(function (err) {
      if (err) next(err);
      return res.redirect("/");
    });
  });
});

app.get("/about", (req, res) => {
  return res.render("about");
});

/**
 * Pour toutes les autres routes non définies, on retourne une erreur
 */
app.all("*", UnknownRoutesHandler);

SyncScheduler.Instance.enable();

SyncScheduler.Instance.execute();

/**
 * Gestion des erreurs
 * /!\ Cela doit être le dernier `app.use`
 */
app.use(ExceptionsHandler);

/**
 * On demande à Express d'ecouter les requêtes sur le port défini dans la config
 */
app.listen(config.PORT, () =>
  console.info(`Server started on port ${config.PORT}`),
);
