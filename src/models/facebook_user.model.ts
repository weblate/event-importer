import {
  Table,
  Column,
  Model,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  DataType,
  AllowNull,
  Index,
} from "sequelize-typescript";
import { User } from "./user.js";

@Table({ tableName: "fb_users" })
export class FacebookUser extends Model {
  @AllowNull(false)
  @Index({
    name: "fb_users_fb_id_user_id",
    unique: true,
  })
  @Column(DataType.STRING)
  declare fbId: string;

  @Column(DataType.STRING)
  declare name: string;

  @Column(DataType.STRING)
  declare email: string;

  @Column(DataType.STRING)
  declare longLivedUserToken: string;

  @CreatedAt
  declare createdAt: Date;

  @UpdatedAt
  declare updatedAt: Date;

  @BelongsTo(() => User, "userId")
  declare user: ReturnType<() => User>;

  @ForeignKey(() => User)
  @Index("fb_users_fb_id_user_id")
  @Column(DataType.INTEGER)
  declare userId: number;
}
