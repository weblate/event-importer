import { IEvent } from "./event.js";

export interface IOrganizer {
  name: string;
  url: string;
  events: IEvent[];
  image?: string;
  description?: string;
}
