import { Destination } from "../models/destination.js";
import { SyncRelation, SyncSource } from "../models/sync.js";
import { Op } from "sequelize";
import { performSync } from "../services/syncher.js";
import { config } from "../config.js";

export class SyncScheduler {
  protected schedulerIntervalMs = 1 * 60 * 1000; // Check for new syncs to perform every 1 min

  private static instance: SyncScheduler;

  // @ts-ignore
  private interval: NodeJS.Timeout;
  private isRunning = false;

  enable() {
    if (!this.schedulerIntervalMs)
      throw new Error("Interval is not correctly set.");

    this.interval = setInterval(() => this.execute(), this.schedulerIntervalMs);
  }

  disable() {
    clearInterval(this.interval);
  }

  async execute() {
    if (this.isRunning === true) {
      console.info(
        "Do not run scheduler %s because the process is already running.",
        this.constructor.name,
      );
      return;
    }

    this.isRunning = true;
    console.info("Starting SyncScheduler");

    try {
      await this.doPerformSync();
    } catch (err) {
      console.error(err, "Cannot execute %s scheduler.", this.constructor.name);
    } finally {
      this.isRunning = false;
    }
  }

  private async doPerformSync() {
    const syncs = await SyncRelation.findAll({
      include: [Destination, SyncSource],
      where: {
        lastSyncedAt: {
          [Op.or]: {
            [Op.eq]: null,
            [Op.lt]: new Date(
              new Date().getTime() -
                config.REFRESHMENT_PERIOD_HOURS * 3600 * 1000,
            ), // Less than REFRESHMENT_PERIOD_HOURS hours (defaults to 2, min to 1),
          },
        },
      },
    });
    console.debug(`Found ${syncs.length} syncs to perform`);
    syncs.forEach(this.handleSync);
  }

  async handleSync(sync: SyncRelation) {
    await performSync(sync);
  }

  static get Instance() {
    return this.instance || (this.instance = new this());
  }
}
