import { Router } from "express";
import { body, validationResult } from "express-validator";
import { Destination } from "../models/destination.js";
import { crawlURL } from "../services/crawl.js";
import { crawlResultIsCrawlOrganizerType } from "../models/crawl_result.js";
import { createEvent } from "ics";
import { EventStatus } from "../models/event.js";
import type { EventAttributes } from "ics";
import { convert } from "html-to-text";

const CrawlController = Router();

CrawlController.get("/", (req, res) => {
  return res.render("crawl");
});

CrawlController.post("/", body("url").isURL(), async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.flash("error", res.__("No URL given to crawl"));
    return res.render("crawl");
  }

  const url = req.body.url;

  console.debug("Ready to proceed, crawling URL");
  const result = await crawlURL(url);
  if (crawlResultIsCrawlOrganizerType(result)) {
    if (result.organizer) {
      return res.render("crawl/organizer", result);
    } else {
      res.flash(
        "error",
        res.__("No event or organizer data found at this URL"),
      );
      return res.render("crawl");
    }
  }
  let destinations;

  const event = result.event;

  const user = req.session.user;
  if (user) {
    destinations = await Destination.findAll({
      where: {
        userId: user.id,
      },
    });
  }

  if (!result.event) {
    res.flash("error", res.__("No event not found at this URL"));
    return res.render("crawl");
  }

  let ics;

  if (event?.startDate && event?.endDate) {
    const params: EventAttributes = {
      start: [
        event?.startDate.getUTCFullYear(),
        event?.startDate.getUTCMonth() + 1,
        event?.startDate.getUTCDate(),
        event.startDate.getUTCHours(),
        event.startDate.getUTCMinutes(),
      ],
      startInputType: "utc",
      end: [
        event?.endDate.getUTCFullYear(),
        event?.endDate.getUTCMonth() + 1,
        event?.endDate.getUTCDate(),
        event.endDate.getUTCHours(),
        event.endDate.getUTCMinutes(),
      ],
      endInputType: "utc",
      title: event?.name,
      description: convert(event?.description),
      location: event?.location?.name,
      status:
        event?.eventStatus && event?.eventStatus === EventStatus.CANCELLED
          ? "CANCELLED"
          : "CONFIRMED",
      categories: event?.tags,
    };

    if (event?.location?.address?.geom) {
      const [lon, lat] = event?.location?.address?.geom?.split(";", 2);
      params.geo = { lon: Number(lon), lat: Number(lat) };
    }

    createEvent(params, (error, value) => {
      ics = {
        value,
        name: `${event.name.replace(/[^a-z0-9]/gi, "_").toLowerCase()}.ics`,
      };
    });
  }

  return res.render("crawl/event", {
    originUrl: result.originUrl,
    event,
    destinations,
    ics,
  });
});

export { CrawlController };
