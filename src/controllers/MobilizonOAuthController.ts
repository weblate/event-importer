import { Router } from "express";
import { AuthorizationCode } from "simple-oauth2";
import {
  MobilizonOAuthApplication,
  MobilizonOAuthApplicationToken,
} from "../models/mobilizon_oauth_application.model.js";
import { registerApplication, userInfo } from "../services/mobilizon.js";
import { downloadURL } from "../utils/http.js";
import { User } from "../models/user.js";
import { endpointUrl, httpProtocol } from "../router.js";

const MobilizonOAuthController = Router();

MobilizonOAuthController.get("/", (req, res) => {
  return res.render("mobilizon/index", { title: "Hello" });
});

MobilizonOAuthController.post("/apps", async (req, res) => {
  const federatedIdentity = req.body.federated_identity as string;
  const domain = federatedIdentity.split("@")[1];

  req.session.federatedIdentity = federatedIdentity;

  let application = await MobilizonOAuthApplication.findOne({
    where: {
      hostname: domain,
    },
  });

  if (!application) {
    const app = await registerApplication(
      domain,
      "Mobilizon Importer",
      [`${endpointUrl(req)}/mobilizon/oauth/callback`],
      "read write",
      "somewhere.org",
    );

    if (!app) {
      res.flash(
        "error",
        res.__("Error while registering application at {{domain}}", { domain }),
      );
      return req.session.save(() => res.redirect("/"));
    }

    application = new MobilizonOAuthApplication({
      name: app.name,
      client_id: app.client_id,
      client_secret: app.client_secret,
      scopes: app.scope,
      hostname: domain,
    });

    await application.save();
  }

  const config = {
    client: {
      id: application.client_id,
      secret: application.client_secret,
    },
    auth: {
      tokenHost: `${httpProtocol()}://${domain}`,
      tokenPath: `/oauth/token`,
    },
  };

  const client = new AuthorizationCode(config);

  const authorizationUri = client.authorizeURL({
    redirect_uri: `${endpointUrl(req)}/mobilizon/oauth/callback`,
    scope: application.scopes,
    state: "hello",
  });
  return res.redirect(authorizationUri);
});

MobilizonOAuthController.get("/callback", async (req, res) => {
  const returned_client_id = req.query["client_id"] as string;

  const application = await MobilizonOAuthApplication.findOne({
    where: {
      client_id: returned_client_id,
    },
  });

  if (!application) {
    res.flash(
      "error",
      res.__("Error while finding application matching this client ID"),
    );
    return req.session.save(() => res.redirect("/"));
  }

  const code = req.query["code"];
  const scope = req.query["scope"];
  const tokenParams = {
    code: code as string,
    redirect_uri: `${endpointUrl(req)}/mobilizon/oauth/callback`,
    scope: scope as string,
    client_id: application.client_id,
    client_secret: application.client_secret,
  };

  try {
    console.debug("getting token", tokenParams);

    const result = await downloadURL(
      `${httpProtocol()}://${application.hostname}/oauth/token`,
      {
        method: "POST",
        body: JSON.stringify({
          ...tokenParams,
          grant_type: "authorization_code",
        }),
        headers: { "Content-Type": "application/json" },
      },
    );

    // const accessToken = await client.getToken(tokenParams);
    const data = (await result.json()) as {
      access_token: string;
      expires_in: number;
      refresh_token: string;
      refresh_token_expires_in: number;
      scope: string;
      token_type: "bearer";
    };
    console.debug("auth token data", data);
    const federatedIdentity = req.session.federatedIdentity;
    console.debug("Using federatedIdentity from session", federatedIdentity);
    const authToken = new MobilizonOAuthApplicationToken({
      accessToken: data["access_token"],
      accessTokenExpiresIn: data["expires_in"],
      refreshToken: data["refresh_token"],
      refreshTokenExpiresIn: data["refresh_token_expires_in"],
      scopes: data["scope"],
      appId: application.id,
      federatedIdentity: federatedIdentity ?? null,
    });
    await authToken.save();
    const user = await userInfo(application.hostname, authToken);

    let dbUser = await User.findOne({
      where: {
        email: user.email,
      },
    });
    if (!dbUser) {
      dbUser = new User({
        email: user.email,
        name: user.defaultActor.name,
        sourceType: "mobilizon",
      });
      dbUser = await dbUser.save();
    }
    // Remove all older tokens for this user
    await MobilizonOAuthApplicationToken.destroy({
      where: {
        userId: dbUser.id,
      },
    });
    // attribute the new token to our user
    await authToken.update({ userId: dbUser.id });

    req.session.regenerate(function (err) {
      req.session.user = {
        // @ts-expect-error
        id: dbUser.id,
        email: user.email,
        name: user.defaultActor.name,
        username: user.defaultActor.preferredUsername,
        avatar: user.defaultActor?.avatar?.url,
      };
      res.flash("success", res.__("Successfully connected to Mobilizon"));
      req.session.save((err) => {
        return res.redirect("/");
      });
    });
  } catch (error: any) {
    console.error("Access Token Error", error.message);
    return res.status(500).json(error.output);
  }
});

export { MobilizonOAuthController };
