import { Sequelize } from "sequelize-typescript";
import {
  Destination,
  FacebookUser,
  MobilizonOAuthApplication,
  MobilizonOAuthApplicationToken,
  SyncRelation,
  SyncRelationAction,
  SyncSource,
  WebHook,
} from "./models/index.js";
import { User } from "./models/user.js";

import configs from "../config/config.json" assert { type: "json" };

const environment = process.env.NODE_ENV || "development";

// @ts-expect-error
const config = configs[environment];

export const sequelize = new Sequelize({
  ...config,
  logging: environment === "development" ? console.log() : false,
  models: [
    Destination,
    FacebookUser,
    MobilizonOAuthApplication,
    MobilizonOAuthApplicationToken,
    SyncSource,
    SyncRelation,
    SyncRelationAction,
    User,
    WebHook,
  ],
});
